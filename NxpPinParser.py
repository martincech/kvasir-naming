import json
import argparse
import sys
import csv
import os
from os import path as posixpath


def mergedicts(dict1, *dicts):
   for dict2 in dicts:
      dict1.update(dict2)
   return dict1

def fieldEnums(fields, enum):
   ret = {}
   enums={}
   for a, b in enum.items():
      mergedicts(enums, { 
            a: {".rename":b}
         })

   for field in fields:
      mergedicts(ret, {
         field: {
            "enum" : enums
         }
      })
   return ret

def wdog():
   fieldens = fieldEnums(["STOP","WAIT", "DBG", "UPDATE", "INT", "EN", "PRES", "CMD32EN", "WIN"],{
                        "0": "disabled",
                        "1": "enabled"
                     })
   out = {
      ".WDOG":{
         "register": {
            "CS": {
               "field": mergedicts(fieldens, {                 
                  "CLK": {
                     "enum": {
                        "00": {
                           ".rename": "bus"
                        },
                        "01": {
                           ".rename": "lpo"
                        },
                        "10": {
                           ".rename": "intclk"
                        },
                        "11": {
                           ".rename": "erclk"
                        }
                     }
                  },
                  "RCS": {
                     "enum": {
                        "0": {
                           ".rename": "reconfiguring"
                        },
                        "1": {
                           ".rename": "reconfigurationSuccess"
                        }
                     }
                  },
                  "ULK": {
                     "enum": {
                        "0": {
                           ".rename": "locked"
                        },
                        "1": {
                           ".rename": "unlocked"
                        }
                     }
                  },                  
                  "FLG": {
                     "enum": {
                        "0": {
                           ".rename": "interruptNotDetected"
                        },
                        "1": {
                           ".rename": "interruptDetected"
                        }
                     }
                  }                  
               })
            },
            "TOVAL":{
               "additional_fields":[
               {
                  "name":"toval32",
                  "description":"32 bit access to the TOVAL register",
                  "bit_offset": 0,
                  "bit_width": 32,
                  "access": "read-write",
                  "modified_write_values":"modify",
                  "read_action": None,
                  "enumerated_values":None
               }
               ]
            },
            "Win":{
               "additional_fields":[
               {
                  "name":"win32",
                  "description":"32 bit access to the WIN register",
                  "bit_offset": 0,
                  "bit_width": 32,
                  "access": "read-write",
                  "modified_write_values":"modify",
                  "read_action": None,
                  "enumerated_values":None
               }
               ]
            },
            "CNT":{    
               "field": {
                  "cnt32": {
                     "enum": {
                        "unlock": {
                           ".rename": "unlock"
                        },
                        "refresh": {
                           ".rename": "refresh"
                        }
                     }
                  }
               },            
               "additional_fields":[
                  {
                     "name":"cnt32",
                     "description":"32 bit access to the CNT register",
                     "bit_offset": 0,
                     "bit_width": 32,
                     "access": "write-only",
                     "modified_write_values":"modify",
                     "read_action": None,
                     "enumerated_values":[
                        {
                           "name": "unlock",
                           "value": 0xD928C520,
                           "is_default": None,
                           "description": "The 32-bit value used for unlocking the WDOG."
                        },
                        {
                           "name": "refresh",
                           "value": 0xB480A602,  
                           "is_default": None,
                           "description": "The 32-bit value used for resetting the WDOG counter."
                        }
                     ]
                  }
               ]            
            }
         }

      }
   }   
   return out

def peripheralClock():
   pcsEnum = {}
   for i in range(0, 8):
      if i is 0:
         name = "off"
      else:
         name="option"+str(i)
      pcsEnum = mergedicts(pcsEnum, {
         '{0:03b}'.format(i): {
            ".rename": name
         }
      })

   out = {
      ".PCC":{
         "field": {
            "CGC": {
               "enum": {
                  "0": {
                     ".rename": "disabled"
                  },
                  "1": {
                     ".rename": "enabled"
                  }
               }
            },
            "PCS": {
               "enum": pcsEnum
            },
            "PR": {
               "enum": {
                  "0": {
                     ".rename": "missing"
                  },
                  "1": {
                     ".rename": "present"
                  }
               }
            }
         }
      }
   }   
   return out

def isfEnum():
   return {
      "0": {
         ".rename": "interruptNotDetected"
      },
      "1": {
         ".rename": "interruptDetected"
      }
   }
def irqcEnum():
   enum={}
   for i in [4,5,6,7,13,14,15]:
      enum = mergedicts(enum, {
         '{0:04b}'.format(i): {
            ".rename": "reserved"
         }
      })
   enum = mergedicts(enum, {
      '{0:04b}'.format(0): {
         ".rename": "disabled"
      }
   })
   enum = mergedicts(enum, {
      '{0:04b}'.format(1): {
         ".rename": "isfDma_on_risingEdge"
      }
   })
   enum = mergedicts(enum, {
      '{0:04b}'.format(2): {
         ".rename": "isfDma_on_fallingEdge"
      }
   })
   enum = mergedicts(enum, {
      '{0:04b}'.format(3): {
         ".rename": "isfDma_on_eitherEdge"
      }
   })
   enum = mergedicts(enum, {
      '{0:04b}'.format(8): {
         ".rename": "isf_on_levelLow"
      }
   })
   enum = mergedicts(enum, {
      '{0:04b}'.format(9): {
         ".rename": "isf_on_risingEdge"
      }
   })
   enum = mergedicts(enum, {
      '{0:04b}'.format(10): {
         ".rename": "isf_on_fallingEdge"
      }
   })
   enum = mergedicts(enum, {
      '{0:04b}'.format(11): {
         ".rename": "isf_on_eitherEdge"
      }
   })
   enum = mergedicts(enum, {
      '{0:04b}'.format(12): {
         ".rename": "isf_on_levelHigh"
      }
   })
   return enum

def pinMuxingFromFile(file):
   out = {}
   fileReader = csv.DictReader(
       open(file, newline="\n"), delimiter=";", quotechar="\"")
   for row in fileReader:
      pinName = row["Pin Name"]
      if not pinName.startswith("PT"):
         continue
      portName = pinName[2]
      pinNum = int(pinName[3:])
      registers = {}
      enum = {}

      for i in range(0, 8):         
         if i is 0:
            fnName = "disabled"
         elif i is 1:
            fnName = "gpio"
         elif row["ALT"+str(i)] == '':
            continue
         else:
            fnName =  row["ALT"+str(i)]
         fnName = fnName.replace("/", "_")
         fnName = fnName.replace("[", "_")
         fnName = fnName.replace("]", "_")
         if fnName.startswith("ADC"):
            fnName="analog"

         enum = mergedicts(enum, {
            '{0:03b}'.format(i): {
               ".rename": fnName.lower()
            }
         })

      PCR = {"field": {
         "MUX": {
            "enum": enum
         },
         "IRQC": {
            "enum": irqcEnum()
         },
         "ISF": {
            "enum": isfEnum()
         }
      }
      }
      register = {
         "PCR"+str(pinNum): PCR
      }
      
      name = ".PORT"+portName
      if name in out and "PCR"+str(pinNum) in out[name]["missing_register"]:
         del out[name]["missing_register"]["PCR"+str(pinNum)]

      if name in out:
         mergedicts(out[name]["register"], register)
      else:
         allPcrs={}
         for n in range(0,32):
            mergedicts(allPcrs, {
               "PCR"+str(n):{}
            })
         del allPcrs["PCR"+str(pinNum)]
         mergedicts(out, {
            name: {
               "missing_register": allPcrs,
               "register": register,
               "field": {
                  "ISF": {
                     "enum": isfEnum()
                  },
                  "IRQC": {
                     "enum": irqcEnum()
                  },
                  "DSE": {
                     "enum": {
                        "0": {
                           ".rename": "lowDriveStrength"
                        },
                        "1": {
                           ".rename": "highDriveStrength"
                        }
                     }
                  },
                  "PE": {
                     "enum": {
                        "0": {
                           ".rename": "pullDisable"
                        },
                        "1": {
                           ".rename": "pullEnable"
                        }
                     }
                  },
                  "PS": {
                     "enum": {
                        "0": {
                           ".rename": "pullDown"
                        },
                        "1": {
                           ".rename": "pullUp"
                        }
                     }
                  },
                  "LK": {
                     "enum": {
                        "0": {
                           ".rename": "registerNotLocked"
                        },
                        "1": {
                           ".rename": "registerLocked"
                        }
                     }
                  },
               }
            }
         })      

   mergedicts(out)
   return out


def kvasirIo():
   return {
      "kvasir": {
         "ioctrl": {
            "default": {
               "type": "groupgroup",
               "ports": "A-E",
               "peripheral": "PORT%s",               
               "pins": "0-31",
               "register":"PCR%s"
           },
           "Gpio":{
               "field":"MUX",
               "value": "001",
            },
            "InterruptenFalling":{
               "field":"IRQC",
               "value": "1010",
            },
            "InterruptenRising":{
               "field":"IRQC",
               "value": "1001",
            },
            "InterruptenAlternating":{
               "field":"IRQC",
               "value": "1011",
            },
            "InterruptProcess":{
               "field":"ISF",
               "value": "1",
            },
            "InterruptRead":{
               "field":"ISF",
               "value": "1",
            }
         },
         "ioclk":{
            "default": {
               "type": "groupgroup",
               "ports": "A-E",
               "peripheral": "PORT%s",
               "register": "PCC"
            },
            "ClockEn":{
               "field": "CGC",
               "value": "1"
            }
         },
        "io": {
           "default": {
               "type": "group",
               "ports": "A-E",
               "peripheral": "PT%s"
           },
           "output": {
               "value": 1,
               "register": "PDDR"
           },
           "input": {
               "value": 0,
               "register": "PDDR"
           },
           "set": {
               "value": 1,
            "register": "PSOR"
           },
           "clear": {
               "value": 1,
               "register": "PCOR"
           },
           "toggle": {
               "value": 1,
               "register": "PTOR"
           },
           "read": {
               "register": "PDIR"
           },
           "write": {
               "register": "PDOR"
           }
         }
     }
   }

def adcChannels():
   out = {}
   for adc in range(0, 2):
      name = ".ADC"+str(adc)
      register = {}
      for sc in range(ord('A'), ord('Z')):
         # ext channels
         enum = {}
         for j in [0, 16]:
            for i in range(0, 16):
               fnName = "ExtChannel"+str(i+j)
               n = i
               if j is not 0:
                  n = i+32
               form = '{0:06b}'
               if i+j == 9:
                  form = '{0:05b}'
               enum = mergedicts(enum, {
                  form.format(n): {
                     ".rename": fnName.lower()
                  }
               })
         # int channels
         for i in range(21, 24):
            fnName = "IntChannel"+str(i-21)
            enum = mergedicts(enum, {
               '{0:06b}'.format(i): {
                  ".rename": fnName.lower()
               }
            })
         # others
         for i in [[26, 'TempSensor'], [27, 'BandGap'], [28, 'IntChannel3'], [29, 'VrefSH'], [30, 'VrefSL']]:
            fnName = i[1]
            enum = mergedicts(enum, {
               '{0:06b}'.format(i[0]): {
                  ".rename": fnName.lower()
               }
            })
         ADCH = {
                  "enum": enum
               }
         aienEnum={}
         for i in [[0, 'Disabled'], [1, 'Enabled']]:
            aienEnum = mergedicts(aienEnum, {
               '{0:01b}'.format(i[0]): {
                  ".rename": i[1].lower()
               }
            })
         
         cocoEnum={}
         for i in [[0, 'not_completed'], [1, 'completed']]:
            cocoEnum = mergedicts(cocoEnum, {
               '{0:01b}'.format(i[0]): {
                  ".rename": i[1].lower()
               }
            })

         SC1 = {
            "field": {
               "ADCH": {
                  "enum": enum
               },
               "AIEN":{
                  "enum": aienEnum
               },
               "COCO":{
                  "enum": cocoEnum
               },
            }
         }
         dmaenum={}
         for i in [[0, 'disabled'], [1, 'enabled']]:
            dmaenum = mergedicts(dmaenum, {
               '{0:01b}'.format(i[0]): {
                  ".rename": i[1].lower()
               }
            })
         verfval={}
         for i in [[0, 'def'], [1, 'alternate']]:
            verfval = mergedicts(verfval, {
               '{0:02b}'.format(i[0]): {
                  ".rename": i[1].lower()
               }
            })

         cmpval={}
         for i in [[0, 'disabled'], [1, 'enabled']]:
            cmpval = mergedicts(cmpval, {
               '{0:01b}'.format(i[0]): {
                  ".rename": i[1].lower()
               }
            })
         SC2={
            "field": {
               "DMAEN": {
                  "enum": dmaenum
               },
               "REFSEL":{
                  "enum": verfval
               },
               "ACFE":{
                  "enum": cmpval
               },
            }
         }

         rd={
            "SC1"+chr(sc): SC1,
            "aSC1"+chr(sc): SC1, 
            "SC2": SC2,
         }
         if sc < ord('P'):
            rd=mergedicts(rd, {"SC1A"+chr(sc): SC1})
         
         register = mergedicts(register, rd) 

      mergedicts(out, {
         name: {
            "register": register
         }
      })
   return out

parser = argparse.ArgumentParser(description='Generate chip files')
parser.add_argument('-f,--file', dest='file', help='file name to parse')
parser.add_argument('-o,--output', dest='path', help='file name to write the result')

args = parser.parse_args()
if args.file is None or args.path is None:
   parser.print_help()
   exit()

out = {}
mergedicts(out, pinMuxingFromFile(args.file))
mergedicts(out, kvasirIo())
mergedicts(out, adcChannels())
mergedicts(out, peripheralClock())
mergedicts(out, wdog())


outFile = open(args.path, "w", encoding='utf-8')
outFile.write(str(json.dumps(out, indent=3)))
